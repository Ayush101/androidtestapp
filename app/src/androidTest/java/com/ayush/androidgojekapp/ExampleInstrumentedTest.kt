package com.ayush.androidgojekapp

import android.R
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.ayush.androidgojekapp.inject.GitRepoModel
import com.ayush.androidgojekapp.ui.TredingActivity
import okhttp3.internal.tls.OkHostnameVerifier.verify
import org.hamcrest.Matchers.*
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.ayush.androidgojekapp", appContext.packageName)

    }
//     Preferred JUnit 4 mechanism of specifying the activity to be launched before each test
@Rule
var activityTestRule: ActivityTestRule<TredingActivity?>? = ActivityTestRule(TredingActivity::class.java)


    @Mock
    lateinit var user: GitRepoModel
    lateinit var tested: GitRepoModel

    @Test
    fun englishGreetIsCorrect() {
        Mockito.`when`(user.author).thenReturn("hello")
        assertEquals("Hello ayush", tested.author)
    }

}
