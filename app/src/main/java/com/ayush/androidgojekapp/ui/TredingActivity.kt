package com.ayush.androidgojekapp.ui

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.ayush.androidgojekapp.GitRepoList
import com.ayush.androidgojekapp.inject.GitRepoModel
import com.ayush.androidgojekapp.R
import com.ayush.androidgojekapp.inject.TredingListsDao
import com.ayush.androidgojekapp.inject.network.InternetConnectionListener
import com.ayush.androidgojekapp.presenter.TredingPresenterImpl
import kotlinx.android.synthetic.main.activity_main.*


class TredingActivity : BaseActivity(), TredingView {

    var gitRepoModel: List<GitRepoModel>? = null
    var postPresenter: TredingPresenterImpl? = null

    val tredingmodle: TredingListsDao? = null;

    var cd: ConnectionDetector? = null
    override fun setLayout(): Int {
        return R.layout.activity_main
    }

    override fun init(savedInstanceState: Bundle?) {
        //  postPresenter.getAllPosts()
        cd = ConnectionDetector()
        cd?.isConnectingToInternet(this)
        if (cd?.isConnectingToInternet(this)!!) {
            noconnection.visibility = View.GONE
            connection.visibility = View.VISIBLE
            getPresenter()?.let {
                it.getAllPosts()
            }
        } else {
            noconnection.visibility = View.VISIBLE
            connection.visibility = View.GONE
        }


        swipeContainer?.setOnRefreshListener {
            cd?.isConnectingToInternet(this)
            if (cd?.isConnectingToInternet(this)!!) {
                noconnection.visibility = View.GONE
                connection.visibility = View.VISIBLE
                getPresenter()?.let {
                    it.getAllPosts()
                }
            } else {
                noconnection.visibility = View.VISIBLE
                connection.visibility = View.GONE
            }
            swipeContainer.isRefreshing = false
        }
        retry_txt.setOnClickListener { view ->

            if (cd?.isConnectingToInternet(this)!!) {
                noconnection.visibility = View.GONE
                connection.visibility = View.VISIBLE
                getPresenter()?.let {
                    it.getAllPosts()
                }
            } else {
                noconnection.visibility = View.VISIBLE
                connection.visibility = View.GONE
            }
        }
    }

    class ConnectionDetector {


        fun isConnectingToInternet(context: Context): Boolean {
            val connectivity = context.getSystemService(
                Context.CONNECTIVITY_SERVICE
            ) as ConnectivityManager
            if (connectivity != null) {
                val info = connectivity.allNetworkInfo
                if (info != null)
                    for (i in info)
                        if (i.state == NetworkInfo.State.CONNECTED) {
                            return true
                        }
            }
            return false
        }
    }

    fun getPresenter(): TredingPresenterImpl? {
        postPresenter = TredingPresenterImpl(
            this,
            application
        )
        return postPresenter
    }

    override fun onStartScreen() {
    }

    override fun stopScreen() {
        postPresenter?.let {
            postPresenter!!.unbindView()
            postPresenter = null
        }

    }

    override fun showAllPosts(postList: List<GitRepoModel>) {
        gitRepoModel = postList

        for (i in 0 until postList.size) {
            tredingmodle?.insertRepoListr(
                GitRepoList(
                    postList.get(i).author,
                    postList.get(i).name,
                    postList.get(i).avatar,
                    postList.get(i).url,
                    postList.get(i).description,
                    postList.get(i).language,
                    postList.get(i).languageColor,
                    postList.get(i).currentPeriodStars,
                    postList.get(i).stars,
                    postList.get(i).currentPeriodStars,
                    postList.get(i).builtBy
                )
            )
        }

        Log.d("Response", "" + postList)
        rvtreding.layoutManager = LinearLayoutManager(this)
        rvtreding.adapter =
            TredingItemAdapter(
                postList,
                this
            )
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    // actions on click menu items
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_sortstar -> {

            val sortedDatesDescending = gitRepoModel?.sortedByDescending { it.stars }
            Log.d("Response", "" + sortedDatesDescending)
            rvtreding.layoutManager = LinearLayoutManager(this)
            rvtreding.adapter =
                sortedDatesDescending?.let {
                    TredingItemAdapter(
                        it,
                        this
                    )
                }
            true
        }
        R.id.action_sortname -> {
            val sortedDatesDescending = gitRepoModel?.sortedByDescending { it.name }
            Log.d("Response", "" + sortedDatesDescending)
            rvtreding.layoutManager = LinearLayoutManager(this)
            rvtreding.adapter =
                sortedDatesDescending?.let {
                    TredingItemAdapter(
                        it,
                        this
                    )
                }
            true
        }
        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()

    }

    override fun onDestroy() {
        super.onDestroy()
    }

}