package com.ayush.androidgojekapp.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ayush.androidgojekapp.inject.GitRepoModel
import com.ayush.androidgojekapp.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_view.view.*

class TredingItemAdapter (val gitrepomodel: List<GitRepoModel>, val context: Context) : RecyclerView.Adapter<TredingItemAdapter.ViewHolder>() {

    private val expandedPositionSet: HashSet<Int> = HashSet()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_view, parent, false)
        )

    }

    override fun getItemCount(): Int {
        return gitrepomodel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder?.itemView.bookname.text = gitrepomodel.get(position).author
        holder?.itemView.languagename.text = gitrepomodel.get(position).name
        holder?.itemView.discription.text = gitrepomodel.get(position).description
        holder?.itemView.stars.text = gitrepomodel.get(position).stars.toString()
        holder?.itemView.fork.text = gitrepomodel.get(position).forks.toString()
        holder?.itemView.language.text = gitrepomodel.get(position).language
        Glide.with(context)
            .load(gitrepomodel.get(position).avatar)
            .into(holder?.itemView.userprofile)




        // Expand when you click on cell
        holder.itemView.expand_layout.setOnExpandListener(object :
            ExpandableLayout.OnExpandListener {
            override fun onExpand(expanded: Boolean) {
                if (expandedPositionSet.contains(position)) {
                    expandedPositionSet.remove(position)
                } else {
                    expandedPositionSet.add(position)
                }
            }
        })

        holder.itemView.expand_layout.setExpand(expandedPositionSet.contains(position))

    }



    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }
}