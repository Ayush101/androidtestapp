package com.ayush.androidgojekapp.ui

import com.ayush.androidgojekapp.inject.GitRepoModel
import com.ayush.androidgojekapp.inject.network.IView

interface TredingView: IView {

    fun showAllPosts(postList: List<GitRepoModel>)
}