package com.ayush.androidgojekapp.presenter

import android.app.Application
import com.ayush.androidgojekapp.ui.TredingView
import com.es.developine.ApplicationClass
import com.es.developine.network.INetworkApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.internal.schedulers.IoScheduler
import javax.inject.Inject

class TredingPresenterImpl (var postView: TredingView, var applicationComponent: Application) : TredingPresenter,
    Preseneter<TredingView>(postView) {

    @Inject
    lateinit var mNetworkApi: INetworkApi

    init {
        (applicationComponent as ApplicationClass).applicationComponent.inject(this)
    }

    override fun getAllPosts() {
        var view=view()

        view?.let {
            it.showLoading()
            var allPosts = mNetworkApi.getAllPosts()
            addDisposable(allPosts.subscribeOn(IoScheduler()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        view?.let {
                            it.hideLoading()
                            postView.showAllPosts(result)

                        }
                    },
                    { error ->
                        view?.let {
                            it.hideLoading()

                        }
                    }

                )
            ) }

         if(mNetworkApi.getAllPosts().isEmpty != null){
             view?.let{
                 it.hideLoading()
             }
         }
        var allPosts = mNetworkApi.getAllPosts()
        allPosts.subscribeOn(IoScheduler()).observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                postView.showAllPosts(it)
            }

    }


}