package com.ayush.androidgojekapp.CustomFont

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

class RobotoBoldTextView  : AppCompatTextView {
    constructor(context: Context) : super(context) {
        applyCustomFont(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        applyCustomFont(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyle: Int
        ) : super(context, attrs, defStyle) {
        applyCustomFont(context)
    }

    private fun applyCustomFont(context: Context) {
        
        val normalTypeface =
            Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Bold.ttf")
        this.setTypeface(normalTypeface)
        this.setLineSpacing(0.1f, 1.0f)
        /*Typeface customFont = FontCache.getTypeface("Roboto-Regular.ttf", context);
		setTypeface(customFont);*/
    } 
}