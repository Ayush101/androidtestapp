package com.ayush.androidgojekapp

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ayush.androidgojekapp.inject.BuiltBy


@Entity(tableName = "GitRepoList")
data class GitRepoList(@PrimaryKey val author: String,
                       val name: String,
                       val avatar: String,
                       val url: String,
                       val description: String,
                       val language: String,
                       val languageColor: String,
                       val stars: Int,
                       val forks: Int,
                       val currentPeriodStars: Int,
                       val builtBy: List<BuiltBy>
)


data class BuiltBy( val username:  String,
                  val href: String,
                    val avatar: String )
