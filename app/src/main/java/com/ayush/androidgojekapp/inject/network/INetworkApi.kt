package com.es.developine.network

import com.ayush.androidgojekapp.inject.GitRepoModel
import io.reactivex.Observable
import retrofit2.http.GET

interface INetworkApi {

    @GET(Endpoints.repositories)
    fun getAllPosts(): Observable<List<GitRepoModel>>
}