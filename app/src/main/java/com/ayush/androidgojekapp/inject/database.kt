package com.ayush.androidgojekapp.inject

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [GitRepoModel::class],
        exportSchema = false ,version = 14)
abstract class TredingDatabase : RoomDatabase() {

    abstract fun tredingDatabaseDao(): TredingListsDao

    override fun clearAllTables() {
//        tredingDatabaseDao().clearDb()

    }
    companion object {
        private var INSTANCE: TredingDatabase? = null

        public fun getInstance(context: Context): TredingDatabase? {
            if (INSTANCE == null) {
                synchronized(TredingDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        TredingDatabase::class.java, "TredingDatabase.db")
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}