package com.es.developine

import android.content.Context
import android.net.ConnectivityManager
import androidx.multidex.MultiDexApplication
import com.ayush.androidgojekapp.inject.module.ApplicationComponent
import com.ayush.androidgojekapp.inject.module.DaggerApplicationComponent
import com.es.developine.di.module.NetModule
import okhttp3.Cache
import okhttp3.OkHttpClient
import java.io.File


open class ApplicationClass  : MultiDexApplication() {

    val DISK_CACHE_SIZE = 10 * 1024 * 1024 // 10 MB
    public lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerApplicationComponent.builder()
            .netModule(NetModule())
            .build()

        applicationComponent.inject(this)
    }

    private fun isInternetAvailable(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    private fun provideOkHttpClient(): OkHttpClient? {
        val okhttpClientBuilder = OkHttpClient.Builder()
        okhttpClientBuilder.cache(getCache());
        return okhttpClientBuilder.build()
    }

    open fun getCache(): Cache? {
        val cacheDir = File(cacheDir, "cache")
        return Cache(cacheDir, DISK_CACHE_SIZE.toLong())
    }


}