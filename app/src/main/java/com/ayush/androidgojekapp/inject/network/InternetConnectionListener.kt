package com.ayush.androidgojekapp.inject.network

interface InternetConnectionListener {
    fun onInternetUnavailable()
    fun onCacheUnavailable()


}