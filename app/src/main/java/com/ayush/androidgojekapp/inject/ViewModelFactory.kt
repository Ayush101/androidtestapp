//package com.ayush.androidgojekapp.inject
//
//import androidx.lifecycle.ViewModel
//import androidx.lifecycle.ViewModelProvider
//import javax.inject.Inject
//import javax.inject.Provider
//
//class ViewModelFactory : ViewModelProvider.Factory {
//
//        var creators: Map<Class<out ViewModel?>?, Provider<ViewModel?>?>? =
//            null
//
//        @Inject
//        fun ViewModelFactory(creators: Map<Class<out ViewModel?>?, Provider<ViewModel?>?>) {
//            this.creators = creators
//        }
//
//
//        /* @NonNull
//    @Override
//    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
//        if (modelClass.isAssignableFrom(HotelViewModel.class)) {
//            return (T) new HotelViewModel(repository);
//        }
//        throw new IllegalArgumentException("Unknown class name");
//    }*/
//
//        /* @NonNull
//    @Override
//    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
//        if (modelClass.isAssignableFrom(HotelViewModel.class)) {
//            return (T) new HotelViewModel(repository);
//        }
//        throw new IllegalArgumentException("Unknown class name");
//    }*/
//        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
//            var creator: Provider<out ViewModel?>? = creators!![modelClass]
//            if (creator == null) {
//                for ((key, value) in creators!!) {
//                    if (modelClass.isAssignableFrom(key!!)) {
//                        creator = value
//                        break
//                    }
//                }
//            }
//            requireNotNull(creator) { "unknown model class $modelClass" }
//            return try {
//                (creator.get() as T?)!!
//            } catch (e: Exception) {
//                throw RuntimeException(e)
//            }
//        }
//    }
//
