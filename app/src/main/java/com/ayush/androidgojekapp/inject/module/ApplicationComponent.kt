package com.ayush.androidgojekapp.inject.module

import com.ayush.androidgojekapp.presenter.TredingPresenterImpl
import com.es.developine.ApplicationClass
import com.es.developine.di.module.NetModule
import dagger.Component

@Component(modules = [AppModule::class, NetModule::class])
interface ApplicationComponent {

    fun inject(mewApplication: ApplicationClass)
    fun inject(mTredingPresenterImpl: TredingPresenterImpl)
}