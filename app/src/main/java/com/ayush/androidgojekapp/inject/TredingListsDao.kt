package com.ayush.androidgojekapp.inject

import androidx.room.*
import com.ayush.androidgojekapp.GitRepoList

@Dao
interface TredingListsDao {

    @Query("SELECT * from  GitRepoList")
    fun getAllRepoList(): List<GitRepoList>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRepoListr(gitrepo: GitRepoList)

    @Query("DELETE from GitRepoList")
    fun clearDb()

    @Delete
    fun deleteRepoList(model: GitRepoList)

    @Update
    fun updateRepoList(model: GitRepoList)


}